/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.ufes.valmiraguiar.pss.atv02;

import com.ufes.valmiraguiar.pss.atv02.presenter.MainPresenter;
import java.io.IOException;

/**
 *
 * @author Valmir Aguiar
 */
public class Main {

    public static void main(String[] args) throws IOException {        
        new MainPresenter();
    }
}
