/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.decorator;

/**
 *
 * @author Valmir Aguiar
 */
public class ComLegenda extends GraficoPessoaDecorator {

    public ComLegenda(GraficoPessoa graficoDecorator, boolean comLegenda) {
        super(graficoDecorator);
        this.legenda = comLegenda;
    }    
    
}
