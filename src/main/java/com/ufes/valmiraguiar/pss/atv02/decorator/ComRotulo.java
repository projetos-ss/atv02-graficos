/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.decorator;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;

/**
 *
 * @author Valmir Aguiar
 */
public class ComRotulo extends GraficoPessoaDecorator{

    public ComRotulo(GraficoPessoa graficoDecorator, boolean comRotulo) {
        super(graficoDecorator);
        this.rotulo = comRotulo;
    }    
}
