/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.decorator;

import com.ufes.valmiraguiar.pss.atv02.model.Pessoa;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;

/**
 *
 * @author Valmir Aguiar
 */
public class ComTitulo extends GraficoPessoaDecorator {

    public ComTitulo(GraficoPessoa graficoDecorator, String titulo) {
        super(graficoDecorator);
        this.titulo = titulo;
    }

}
