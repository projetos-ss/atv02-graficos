/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.decorator;

/**
 *
 * @author Valmir Aguiar
 */
public class ComTituloEixoHorizontal extends GraficoPessoaDecorator{

    public ComTituloEixoHorizontal(GraficoPessoa graficoDecorator, String titulo) {
        super(graficoDecorator);
        this.tituloEixoHorizontal = titulo;
    }
    
    
}
