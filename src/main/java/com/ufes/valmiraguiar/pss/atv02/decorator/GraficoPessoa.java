/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.decorator;

import com.ufes.valmiraguiar.pss.atv02.model.Pessoa;
import java.util.ArrayList;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Valmir Aguiar
 */
public abstract class GraficoPessoa {

    protected ArrayList<Pessoa> pessoas;
    protected String titulo;
    protected boolean rotulo;
    protected boolean legenda;
    protected String tituloEixoVertical;
    protected String tituloEixoHorizontal;
    protected PlotOrientation orientacao;

    public GraficoPessoa(ArrayList<Pessoa> pessoas, String titulo, boolean rotulo, boolean legenda, String tituloEixoVertical, String tituloEixoHorizontal, PlotOrientation orientacao) {
        this.pessoas = pessoas;
        this.titulo = titulo;
        this.rotulo = rotulo;
        this.legenda = legenda;
        this.tituloEixoVertical = tituloEixoVertical;
        this.tituloEixoHorizontal = tituloEixoHorizontal;
        this.orientacao = orientacao;
    }    

    public ChartPanel desenharGrafico(ArrayList<Pessoa> pessoas) {
        CategoryDataset dataSet = this.criarDataSet(pessoas);
        JFreeChart grafico = this.criarGrafico(dataSet);

        ChartPanel chartPanel = new ChartPanel(grafico);

        return chartPanel;
    }

    public CategoryDataset criarDataSet(ArrayList<Pessoa> pessoasList) {
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();

        ArrayList<Integer> pessoasSolteiras = new ArrayList<>();
        ArrayList<Integer> pessoasCasadas = new ArrayList<>();

        ArrayList<Integer> masculinoCasado = new ArrayList<>();
        ArrayList<Integer> masculinoSolteiro = new ArrayList<>();

        ArrayList<Integer> femininoCasado = new ArrayList<>();
        ArrayList<Integer> femininoSolteiro = new ArrayList<>();

        for (Pessoa pessoa : pessoasList) {
            switch (pessoa.getEstadoCivil()) {
                case "Solteiro(a)":
                    pessoasSolteiras.add(1);

                    if (pessoa.getSexo().equals("Masculino")) {
                        masculinoSolteiro.add(1);
                    } else {
                        femininoSolteiro.add(1);
                    }

                    break;
                case "Casado(a)":
                    pessoasCasadas.add(1);

                    if (pessoa.getSexo().equals("Masculino")) {
                        masculinoCasado.add(1);
                    } else {
                        femininoCasado.add(1);
                    }

                    break;
            }
        }

        dataSet.setValue(pessoasSolteiras.size(), "Solteiros(a)", "");
        dataSet.setValue(pessoasCasadas.size(), "Casados(a)", "");
        dataSet.setValue(masculinoSolteiro.size(), "Homens Solteiros", "");
        dataSet.setValue(masculinoCasado.size(), "Homens Casados", "");
        dataSet.setValue(femininoSolteiro.size(), "Mulheres Solteiras", "");
        dataSet.setValue(femininoCasado.size(), "Mulheres Casadas", "");

        return dataSet;
    }
    
    public abstract JFreeChart criarGrafico(CategoryDataset dataSet);

    public ArrayList<Pessoa> getPessoas() {
        return pessoas;
    }

    public String getTitulo() {
        return titulo;
    }

    public boolean isRotulo() {
        return rotulo;
    }

    public boolean isLegenda() {
        return legenda;
    }

    public String getTituloEixoVertical() {
        return tituloEixoVertical;
    }

    public String getTituloEixoHorizontal() {
        return tituloEixoHorizontal;
    }

    public PlotOrientation getOrientacao() {
        return orientacao;
    }
}
