/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.decorator;

import com.ufes.valmiraguiar.pss.atv02.model.Pessoa;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;

/**
 *
 * @author Valmir Aguiar
 */
public abstract class GraficoPessoaDecorator extends GraficoPessoa{

    private final GraficoPessoa graficoDecorator;

    public GraficoPessoaDecorator(GraficoPessoa graficoDecorator) {
        super(
                graficoDecorator.pessoas, 
                graficoDecorator.titulo, 
                graficoDecorator.rotulo, 
                graficoDecorator.legenda, 
                graficoDecorator.tituloEixoVertical, 
                graficoDecorator.tituloEixoHorizontal, 
                graficoDecorator.orientacao
        );
        
        this.graficoDecorator = graficoDecorator;
    }    
    
    @Override
    public JFreeChart criarGrafico(CategoryDataset dataSet) {
        JFreeChart graficoBarras
                = ChartFactory.createBarChart(
                        this.titulo,
                        this.tituloEixoHorizontal,
                        this.tituloEixoVertical,
                        dataSet,
                        this.orientacao,
                        this.legenda,
                        this.rotulo,
                        false
                );
        return graficoBarras;
    }
}
