/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.decorator;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;

/**
 *
 * @author Valmir Aguiar
 */
public class SemCorBarras extends GraficoPessoaDecorator{

    public SemCorBarras(GraficoPessoa graficoDecorator) {
        super(graficoDecorator);
    }

    @Override
    public JFreeChart criarGrafico(CategoryDataset dataSet) {
        
        JFreeChart graficoBarras
                = ChartFactory.createBarChart(
                        this.titulo,
                        this.tituloEixoVertical,
                        this.tituloEixoHorizontal,
                        dataSet,
                        this.orientacao,
                        false,
                        false,
                        false
                );        
        return graficoBarras;
        
    }
    
    
    
}
