/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.grafico;

import com.ufes.valmiraguiar.pss.atv02.decorator.GraficoPessoa;
import com.ufes.valmiraguiar.pss.atv02.model.Pessoa;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Valmir Aguiar
 */
public class GraficoPessoaBarras extends GraficoPessoa {

    public GraficoPessoaBarras(ArrayList<Pessoa> pessoas, String titulo, boolean rotulo, boolean legenda, String tituloEixoVertical, String tituloEixoHorizontal, PlotOrientation orientacao) {
        super(pessoas, titulo, rotulo, legenda, tituloEixoVertical, tituloEixoHorizontal, orientacao);
    }

    @Override
    public JFreeChart criarGrafico(CategoryDataset dataSet) {
        JFreeChart graficoBarras
                = ChartFactory.createBarChart(
                        "",
                        "",
                        "",
                        dataSet,
                        this.orientacao,
                        false,
                        false,
                        false
                );
        return graficoBarras;
    }
    
}
