/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.grafico;

import com.ufes.valmiraguiar.pss.atv02.model.Pessoa;
import java.util.ArrayList;
import org.jfree.chart.plot.PlotOrientation;


public class GraficoPessoaBarrasBuilder {

    private ArrayList<Pessoa> pessoas;
    private String titulo;
    private boolean rotulo;
    private boolean legenda;
    private String tituloEixoVertical;
    private String tituloEixoHorizontal;
    private PlotOrientation orientacao;

    public GraficoPessoaBarrasBuilder() {
    }

    public GraficoPessoaBarrasBuilder pessoas(ArrayList<Pessoa> pessoas) {
        this.pessoas = pessoas;
        return this;
    }

    public GraficoPessoaBarrasBuilder titulo(String titulo) {
        this.titulo = titulo;
        return this;
    }

    public GraficoPessoaBarrasBuilder rotulo(boolean rotulo) {
        this.rotulo = rotulo;
        return this;
    }

    public GraficoPessoaBarrasBuilder legenda(boolean legenda) {
        this.legenda = legenda;
        return this;
    }

    public GraficoPessoaBarrasBuilder tituloEixoVertical(String tituloEixoVertical) {
        this.tituloEixoVertical = tituloEixoVertical;
        return this;
    }

    public GraficoPessoaBarrasBuilder tituloEixoHorizontal(String tituloEixoHorizontal) {
        this.tituloEixoHorizontal = tituloEixoHorizontal;
        return this;
    }

    public GraficoPessoaBarrasBuilder orientacao(PlotOrientation orientacao) {
        this.orientacao = orientacao;
        return this;
    }

    public GraficoPessoaBarras createGraficoPessoaBarras() {
        return new GraficoPessoaBarras(pessoas, titulo, rotulo, legenda, tituloEixoVertical, tituloEixoHorizontal, orientacao);
    }
    
}
