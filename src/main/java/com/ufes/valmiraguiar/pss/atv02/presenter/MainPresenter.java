/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.presenter;

import com.ufes.valmiraguiar.pss.atv02.utils.FileUtils;
import com.ufes.valmiraguiar.pss.atv02.decorator.ComLegenda;
import com.ufes.valmiraguiar.pss.atv02.decorator.ComRotulo;
import com.ufes.valmiraguiar.pss.atv02.decorator.ComTitulo;
import com.ufes.valmiraguiar.pss.atv02.decorator.ComTituloEixoHorizontal;
import com.ufes.valmiraguiar.pss.atv02.decorator.ComTituloEixoVertical;
import com.ufes.valmiraguiar.pss.atv02.decorator.GraficoPessoa;
import com.ufes.valmiraguiar.pss.atv02.grafico.GraficoPessoaBarrasBuilder;
import com.ufes.valmiraguiar.pss.atv02.model.Pessoa;
import com.ufes.valmiraguiar.pss.atv02.model.PessoaCollection;
import com.ufes.valmiraguiar.pss.atv02.view.MainView;
import java.awt.BorderLayout;
import java.io.IOException;
import java.util.ArrayList;
import org.jfree.chart.plot.PlotOrientation;

/**
 *
 * @author Valmir Aguiar
 */
public class MainPresenter {

    private MainView view;
    private PessoaCollection pessoas;
    private GraficoPessoa graficoAtual;
    private final ArrayList<GraficoPessoa> graficosAnteriores = new ArrayList<>();

    public MainPresenter() throws IOException {
        init();
    }

    public void init() {
        //pessoas.csv
        FileUtils fileUtils = new FileUtils();
        pessoas = fileUtils.readCsv("dados/pessoas.csv");

        for (Pessoa p : pessoas.getPessoas()) {
            System.out.println(p.getNome());
            System.out.println(p.getEstadoCivil());
        }

        view = new MainView();
        view.setVisible(true);

        view.getBtnFechar().addActionListener((e) -> {
            fechar();
        });

        view.getBtnRestaurarPadrao().addActionListener((e) -> {
            restaurarPadrao();
        });

        view.getBtnDesfazer().addActionListener((e) -> {
            desfazer();
        });

        view.getCboxChartStyle().addActionListener((e) -> {
            mudarPadraoGrafico();
        });

        view.getCkbTitulo().addActionListener((e) -> {
            checkTitulo();
        });

        view.getCkbLegenda().addActionListener((e) -> {
            checkLegenda();
        });

        view.getCkbTituloEixos().addActionListener((e) -> {
            checkTitulosEixos();
        });

        view.getCkbRotuloDadosPorcentagem().addActionListener((e) -> {
            //To do             
        });

        view.getCkbRotuloDadosValor().addActionListener((e) -> {
            checkRotuloDadosValor();
        });

        view.getCkbRotuloDadosValorPorc().addActionListener((e) -> {
            //To do             
        });

        view.getCkbCorBarras().addActionListener((e) -> {
            //To do             
        });

        view.getCkbCorBarrasGrupo().addActionListener((e) -> {
            //To do            
        });

        view.getCkbGrade().addActionListener((e) -> {
            //To do             
        });

        //Não consegui fazer
        view.getCkbRotuloDadosPorcentagem().setEnabled(false);
        view.getCkbGrade().setEnabled(false);
        view.getCkbCorBarrasGrupo().setEnabled(false);
        view.getCkbRotuloDadosValorPorc().setEnabled(false);
        view.getCkbCorBarras().setEnabled(false);
        //-------------------------------------------

        view.getPanelChart().setLayout(new BorderLayout());

        graficoAtual = new GraficoPessoaBarrasBuilder().orientacao(PlotOrientation.HORIZONTAL).createGraficoPessoaBarras();
        graficosAnteriores.add(graficoAtual);

        view.getPanelChart().add(graficoAtual.desenharGrafico(pessoas.getPessoas()));

        view.pack();
    }

    private void fechar() {
        view.dispose();
    }

    private void mudarPadraoGrafico() {
        try {
            switch (view.getCboxChartStyle().getSelectedItem().toString()) {
                case "Barras Verticais":
                    clearAllSelections();
                    view.getPanelChart().removeAll();

                    graficoAtual = new GraficoPessoaBarrasBuilder().orientacao(PlotOrientation.VERTICAL).createGraficoPessoaBarras();
                    graficosAnteriores.add(graficoAtual);

                    view.getPanelChart().add(graficoAtual.desenharGrafico(pessoas.getPessoas()));

                    view.pack();
                    break;
                case "Barras Horizontais":
                    clearAllSelections();
                    view.getPanelChart().removeAll();

                    graficoAtual = new GraficoPessoaBarrasBuilder().orientacao(PlotOrientation.HORIZONTAL).createGraficoPessoaBarras();
                    graficosAnteriores.add(graficoAtual);

                    view.getPanelChart().add(graficoAtual.desenharGrafico(pessoas.getPessoas()));

                    view.pack();
                    break;
                default:
                    throw new RuntimeException("Erro ao selecionar padrão de gráfico");
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao mudar padrão do gráfico " + e.getMessage());
        }

    }

    private void restaurarPadrao() {
        try {
            view.getCboxChartStyle().setSelectedIndex(0);
            clearAllSelections();

            graficosAnteriores.clear();

            view.getPanelChart().removeAll();

            graficoAtual = new GraficoPessoaBarrasBuilder().orientacao(PlotOrientation.HORIZONTAL).createGraficoPessoaBarras();

            view.getPanelChart().add(graficoAtual.desenharGrafico(pessoas.getPessoas()));

            view.pack();
        } catch (Exception e) {
            throw new RuntimeException("Erro ao restaurar padrao " + e.getMessage());
        }
    }

    private void desfazer() {
        try {
            graficosAnteriores.remove(graficosAnteriores.size() - 1);

            if (!graficosAnteriores.isEmpty()) {
                view.getPanelChart().removeAll();
                view.getPanelChart().add(graficosAnteriores.get(graficosAnteriores.size() - 1).desenharGrafico(pessoas.getPessoas()));
                view.pack();
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao desfazer " + e.getMessage());
        }
    }

    private void checkTitulo() {
        try {
            if (view.getCkbTitulo().isSelected()) {
                view.getPanelChart().removeAll();

                graficoAtual = new ComTitulo(graficoAtual, "PESSOAS");
                graficosAnteriores.add(graficoAtual);

                view.getPanelChart().add(graficosAnteriores.get(graficosAnteriores.size() - 1).desenharGrafico(pessoas.getPessoas()));
                view.pack();
            } else {
                view.getPanelChart().removeAll();

                graficoAtual = new ComTitulo(graficoAtual, "");
                graficosAnteriores.add(graficoAtual);

                view.getPanelChart().add(graficosAnteriores.get(graficosAnteriores.size() - 1).desenharGrafico(pessoas.getPessoas()));
                view.pack();
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao acionar título " + e.getMessage());
        }
    }

    private void checkLegenda() {
        try {
            if (view.getCkbLegenda().isSelected()) {
                view.getPanelChart().removeAll();

                graficoAtual = new ComLegenda(graficoAtual, true);
                graficosAnteriores.add(graficoAtual);

                view.getPanelChart().add(graficosAnteriores.get(graficosAnteriores.size() - 1).desenharGrafico(pessoas.getPessoas()));
                view.pack();
            } else {
                view.getPanelChart().removeAll();

                graficoAtual = new ComLegenda(graficoAtual, false);
                graficosAnteriores.add(graficoAtual);

                view.getPanelChart().add(graficosAnteriores.get(graficosAnteriores.size() - 1).desenharGrafico(pessoas.getPessoas()));
                view.pack();
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao acionar legenda " + e.getMessage());
        }
    }

    private void checkRotuloDadosValor() {
        try {
            if (view.getCkbRotuloDadosValor().isSelected()) {
                view.getPanelChart().removeAll();

                graficoAtual = new ComRotulo(graficoAtual, true);
                graficosAnteriores.add(graficoAtual);

                view.getPanelChart().add(graficosAnteriores.get(graficosAnteriores.size() - 1).desenharGrafico(pessoas.getPessoas()));
                view.pack();
            } else {
                view.getPanelChart().removeAll();

                graficoAtual = new ComRotulo(graficoAtual, false);
                graficosAnteriores.add(graficoAtual);

                view.getPanelChart().add(graficosAnteriores.get(graficosAnteriores.size() - 1).desenharGrafico(pessoas.getPessoas()));
                view.pack();
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao acionar rótulo dos dados " + e.getMessage());
        }
    }

    private void checkTitulosEixos() {
        try {
            if (view.getCkbTituloEixos().isSelected()) {
                view.getPanelChart().removeAll();

                graficoAtual = new ComTituloEixoHorizontal(graficoAtual, "Dados");
                graficoAtual = new ComTituloEixoVertical(graficoAtual, "Quantidade");
                graficosAnteriores.add(graficoAtual);

                view.getPanelChart().add(graficosAnteriores.get(graficosAnteriores.size() - 1).desenharGrafico(pessoas.getPessoas()));
                view.pack();
            } else {
                view.getPanelChart().removeAll();

                graficoAtual = new ComTituloEixoHorizontal(graficoAtual, "");
                graficoAtual = new ComTituloEixoVertical(graficoAtual, "");
                graficosAnteriores.add(graficoAtual);

                view.getPanelChart().add(graficosAnteriores.get(graficosAnteriores.size() - 1).desenharGrafico(pessoas.getPessoas()));
                view.pack();
            }
        } catch (Exception e) {
            throw new RuntimeException("Erro ao acionar título dos eixos " + e.getMessage());
        }
    }

    private void clearAllSelections() {
        view.getCkbTitulo().setSelected(false);
        view.getCkbLegenda().setSelected(false);
        view.getCkbTituloEixos().setSelected(false);
        view.getCkbRotuloDadosPorcentagem().setSelected(false);
        view.getCkbRotuloDadosValor().setSelected(false);
        view.getCkbRotuloDadosValorPorc().setSelected(false);
        view.getCkbCorBarras().setSelected(false);
        view.getCkbCorBarrasGrupo().setSelected(false);
        view.getCkbGrade().setSelected(false);
    }
}
