/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ufes.valmiraguiar.pss.atv02.utils;

import com.ufes.valmiraguiar.pss.atv02.model.Pessoa;
import com.ufes.valmiraguiar.pss.atv02.model.PessoaCollection;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Valmir Aguiar
 */
public class FileUtils {

    public PessoaCollection readCsv(String path) {

        //String text = "";
        PessoaCollection pessoas = new PessoaCollection();

        try ( BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line = br.readLine(); //cabecalho

            while (line != null) { //ler ate o fim do arquivo    
                //text += line;
                String[] vect = line.split(",");
                Pessoa p = new Pessoa(vect[1], vect[0], vect[2]);

                pessoas.addPessoa(p);
                line = br.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        return pessoas;
    }
}
